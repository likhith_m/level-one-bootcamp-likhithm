#include<stdio.h>
struct fraction
{
    int numer;
    int deno;
};

typedef struct fraction fraction;

fraction input()
{
    fraction f;
    printf("Enter the numerator:\n");
    scanf("%d",&f.numer);
    printf("Enter the denominator:\n");
    scanf("%d",&f.deno);
    return f;
}
fraction compute(fraction f1, fraction f2)
{
    int hcf, i, numerator, denominator;
    numerator=(f1.numer*f2.deno)+(f1.deno*f2.numer);
    denominator=f1.deno*f2.deno;
    for(i=1;i<=numerator&&i<=denominator;++i)
    {
        if(numerator%i==0&&denominator%i==0)
        {
            hcf=i;
        }
    }
    fraction sum;
    sum.numer=numerator/hcf;
    sum.deno=denominator/hcf;
    return sum;
}
void output(fraction sum)
{
    printf("The added fraction is %d/%d",sum.numer,sum.deno);
}
int main()
{
    fraction f1,f2,sum;
    printf("for the first fraction \n");
    f1=input();
    printf("for the second fraction \n");
    f2=input();
    sum=compute(f1,f2);
    output(sum);
    return 0;
}
//WAP to find the sum of two fractions.